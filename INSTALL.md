
# Installation Considerations

# Avoid GOPATH
Take care not to clone repo into a location that is in GOPATH.  
If GOPATH was never set, it is probably ~/go.  
So in that case, please make sure this is cloned outside ~/go.

## Manually installed:  
1. Golang >= 1.17  
2. Make > 3.8 if not exists though something should work.  

## Pull Down Golang Based Packages via Go Modules
Once Golang is installed, run `go mod tidy` from repo root.  
This should install all necessary dependencies, including   
the embedded BadgerDB we currently use in the code base as a  
ACID compliant kv store for zones/offsets.  

