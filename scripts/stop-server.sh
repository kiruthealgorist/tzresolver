#!/bin/bash
# Helpder script to kill any running tzresolver process on
# local system
set +e
ps auxww | grep tzresolver | awk '{print $2}' | xargs kill -9 &> /dev/null || true
