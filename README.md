# Timezone Resolver
Please see requirements doc in `requirements/` folder for details on what application does :).  
Application also prints out documentation when it boots up.

## First Steps
1. Please read `INSTALL.md` in repo root to install the necessary dependencies.  
2. Please study the comments targets in `Makefile` in repo root. 
   Running `make targetnet` e.g. `make test` should all be operational once 
   the steps in `INSTALL.md` have been completed. 

## Documention for Actual Application
Any of the make targest that run the server (foreground or background) should spit 
out usage and schema for commands to the screen.  Please follow that.  

## Choices Made
1. TCP connections are persistent, thereby client can continuously send commands 
   line by line.  A reply to a R (read) command will be sent back to client.
   The connection can be broken by client at any time, and server will break  
   it on any error. 
2. Not much logging has been enabled to save coding time and also  
   a broken TCP connection by server is to denote an error.


## Architecture

### Layers:
1. TCP Server Layer 
    - main thread starts N workers (N==num cores, though this is easily changed)
    - main thread continues to listen on the receiving TCP port and keeps accepting connections 
    - each accepted connection is sent on a buffer/queue towards the N workers who keep pulling 
      connections off the queue as soon as their earlier connection was closed (either by
      client or the worker/server itself)
    - the queue size right now is to 4N, though more consideration needs to be done before finding 
      a good balance between the main thread being blocked from accepting more connections,
      and the latencies of accepted connections stuck in the queue before commands are serviced
    - each of the N workers are not just workers that accept TCP connections, but each worker
      also ends up making independent requests into the embedded DB via the layers explained below:
2. API Layer
    - Each worker described in TCP Server Layer calls an API Handler function 
3. TZ Core ("Business Logic" layer as some would call it)
    - API Handler function would in return call a TZ Core function
4. DAL Layer
    - Each TZ Core function would call into the DAL Layer to grab data from data store
5. Embedded DB
    - The DAL functions provide an abstraction over the DB, which makes for easy extensibility, testability,
      and possibly supporting an application that can take a config to pick which actual DB to use
      when interfaced via the DAL

## Benchmark Results
A benchmark suite was built into `src/pkg/tzcore` package.  
The parallel benchmark tests therein simulate using N workers in parallel,  
but skipping TCP Server and API layers described above.  

Results are in repo root's `benchmark_results` 
Very short summary and shallow analysis is found in `benchmark_results/SUMMARY_RESULTS.md`  

TODO: Add some benchmark tests that use a component test setup, with a client process calling  
      into real application process. 
