package codec

import "testing"

func TestEncodeReadResp(t *testing.T) {
	type args struct {
		iso8601times []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Single time list",
			args: args{
				iso8601times: []string{"2021-08-22T20:02:27"},
			},
			want: "2021-08-22T20:02:27",
		},
		{
			name: "Multi time list",
			args: args{
				iso8601times: []string{"2021-08-22T20:02:27", "2021-08-23T00:02:27"},
			},
			want: "2021-08-22T20:02:27;2021-08-23T00:02:27",
		},
		{
			name: "Empty list",
			args: args{
				iso8601times: []string{}, // Precondition prevents this, but kept as test
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EncodeReadResp(tt.args.iso8601times); got != tt.want {
				t.Errorf("EncodeReadResp() = %v, want %v", got, tt.want)
			}
		})
	}
}
