package codec

import "strings"

// EncodeReadResp encodes a list of ISO-8016 times into
// return format used for responses to read queries
// Precondition: Input time list should be non-empty
func EncodeReadResp(iso8601times []string) string {
	return strings.Join(iso8601times, ";")
}
