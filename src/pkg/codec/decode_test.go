package codec

import (
	"reflect"
	"testing"
)

func TestDecodeZoneList(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Single zone list",
			args: args{
				data: "zone1",
			},
			want: []string{"zone1"},
		},
		{
			name: "Multi zone list",
			args: args{
				data: "zone1;zone2;zone3,name",
			},
			want: []string{"zone1", "zone2", "zone3,name"},
		},
		{
			name: "Empty zone list", // Precondition to function disallows
			args: args{
				data: "",
			},
			want: []string{""},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DecodeZoneList(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeZoneList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodeUpdateData(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]int
		wantErr bool
	}{
		{
			name: "Format error: not of form zone,offset",
			args: args{
				data: "zone1,100;badzoneoffset",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Format error: offset not parseable as integer",
			args: args{
				data: "zone1,100;zone2,10.0", // 10.0 not int
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Single zone,offset",
			args: args{
				data: "zone1,-200",
			},
			want: map[string]int{
				"zone1": -200,
			},
			wantErr: false,
		},
		{
			name: "Multiple zone,offset (test offsets with leading +,- and no leading sign)",
			args: args{
				data: "zone1,+120;zone2,-100;zone3,55",
			},
			want: map[string]int{
				"zone1": 120,
				"zone2": -100,
				"zone3": 55,
			},
			wantErr: false,
		},
		{
			name: "Empty zone,offset list", // Precondition to function disallows
			args: args{
				data: "",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodeUpdateData(tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeUpdateData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeUpdateData() = %v, want %v", got, tt.want)
			}
		})
	}
}
