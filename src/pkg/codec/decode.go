package codec

import (
	"fmt"
	"strconv"
	"strings"
)

// DecodeZoneList decodes the data line that we use for
// reads and deletes: zone1;zone2;...;zoneN
// Precondition: data should not be empty string (must be 1 zone at least)
func DecodeZoneList(data string) []string {
	return strings.Split(data, ";")
}

// DecodeUpdateData decodes the data line that we use for
// updates/inserts: zone1,offset1;zone2,offset2;...;zoneN,offsetN
// Precondition: data should not be empty string
func DecodeUpdateData(data string) (map[string]int, error) {
	// Split into list on ; boundaries
	allTzOffsets := strings.Split(data, ";")

	// Holds the result zone->offset map we return
	tzOffsetsMap := make(map[string]int, len(allTzOffsets))

	for _, tzOffset := range allTzOffsets {
		// Split zone,offset on ,
		s := strings.Split(tzOffset, ",")
		if len(s) != 2 {
			return nil, fmt.Errorf("Format error for <%s>, must be of form zone,offset", tzOffset)
		}
		offset, err := strconv.Atoi(s[1])
		if err != nil {
			return nil, fmt.Errorf("Format error for <%s>, offset must be an integer", tzOffset)
		}
		tzOffsetsMap[s[0]] = offset
	}

	return tzOffsetsMap, nil
}
