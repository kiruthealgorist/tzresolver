package badgerdb

import (
	"github.com/dgraph-io/badger"
	"github.com/dgraph-io/badger/options"
)

// Enum of constants for the tableLoadingMode
// argument to InitBadgerDB.
// Specifies how data in LSM table files should be loaded.
// This does not change the ACID properties of txns, but only perf
// and memory usage.
const (
	// FileIO indicates that LSM files must be loaded using standard I/O
	// LSM is stored on disk always (saves on RAM, but more disk I/O)
	FileIO = iota
	// LoadToRAM indicates that LSM files must be loaded into RAM
	// Best perf, but uses most RAM
	LoadToRAM
	// MemoryMap indicates that that LSM files must be memory-mapped
	// Goes through OS virtual memory mapping
	MemoryMap
)

// InitBadgerDB opens/initializes an embedded Badger kv database in read/write mode
// and returns a db handle structure which multiple threads can use do reads/writes to.
// Args:
//  dbFilePath - path to a directory that will be used to store db files (will be created if not exists)
//  syncWrites - writes done to memory first without writing to value log (like a WAL) nor compacted to disk
//               this improves perf at expense of losing ACID properties (e.g. data durability on crashes)
//  tableLoadingMode - must be an enum as defined just above which controls how LSM table values are loaded
func InitBadgerDB(dbFilePath string, syncWrites bool, tableLoadingMode int) (*badger.DB, error) {

	// Get all the recommended default options for DB
	opts := badger.DefaultOptions(dbFilePath)

	// Overwrite some default options as necessary
	opts.TableLoadingMode = options.FileLoadingMode(tableLoadingMode)
	opts.SyncWrites = syncWrites

	// Turn off logging for now until we have a better logging strategy
	opts.Logger = nil

	// Open db in read-write mode:
	// Only 1 thread must open the DB in this mode,
	//  but once opened, multiple threads can use db handled returned
	//  from this function.
	opts.ReadOnly = false

	return badger.Open(opts)
}
