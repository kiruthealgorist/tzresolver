package timeutils

import (
	"reflect"
	"testing"
	"time"
)

func Test_timeFormatISO8601(t *testing.T) {
	type args struct {
		theTime time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			// There is only 1 sub-casetest for this function because it takes a strict
			// Golang time.Time object (which caller has already created, thus is valid)
			name: "Pass in a time.Time object",
			args: args{
				// Create a specific time.Time at a specific moment in time
				theTime: time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
			},
			want: "2009-11-10T23:00:00",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := timeFormatISO8601(tt.args.theTime); got != tt.want {
				t.Errorf("timeFormatISO8601() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_shiftTimeByOffset(t *testing.T) {
	type args struct {
		theTime time.Time
		offset  int
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{
			name: "Positive offset",
			args: args{
				theTime: time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
				offset:  10,
			},
			// time.Time object which we hand craft to be offset by +10 seconds
			want: time.Date(2009, time.November, 10, 23, 0, 10, 0, time.UTC),
		},
		{
			name: "Negative offset",
			args: args{
				theTime: time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
				offset:  -60,
			},
			// time.Time object which we hand craft to be offset by -60 seconds
			want: time.Date(2009, time.November, 10, 22, 59, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shiftTimeByOffset(tt.args.theTime, tt.args.offset); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("shiftTimeByOffset() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestISO8061TimeUsingOffset(t *testing.T) {
	type args struct {
		theTime time.Time
		offset  int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			// Only one sub-case is really needed, as the variations
			// (e.g. positive vs. negative offset) are taken care of by tests
			// of helper functions
			name: "Pass in a time.Time object and offset, expect ISO-8061 output",
			args: args{
				theTime: time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
				offset:  -60,
			},
			want: "2009-11-10T22:59:00",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ISO8061TimeUsingOffset(tt.args.theTime, tt.args.offset); got != tt.want {
				t.Errorf("ISO8061TimeUsingOffset() = %v, want %v", got, tt.want)
			}
		})
	}
}
