package timeutils

import "time"

// timeFormatISO8601 takes a golang time.Time object representing
// any moment in time and returns it formatted in a
// format aligning with ISO-8601
func timeFormatISO8601(theTime time.Time) string {
	return theTime.Format("2006-01-02T15:04:05")
}

// shiftTimeByOffset takes a golang time.Time object
// and returns another time.Time object which has time
// shifted by offset seconds
func shiftTimeByOffset(theTime time.Time, offset int) time.Time {
	return theTime.Add(time.Second * time.Duration(offset))
}

// ISO8061TimeUsingOffset returns a ISO-8601 formatted time associated
// with input time (Golang time.Time object) and the offset from this time
// in seconds.  If offset is positive integer, that many seconds is added to
// time, if negative, subtracted.
func ISO8061TimeUsingOffset(theTime time.Time, offset int) string {
	return timeFormatISO8601(shiftTimeByOffset(theTime, offset))
}
