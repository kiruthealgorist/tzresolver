package tzcore

import (
	"time"
	"tzresolver/src/pkg/codec"
	"tzresolver/src/pkg/dal"
	"tzresolver/src/pkg/timeutils"
)

// iClock is an interface that wraps up a Now() method that is intended
// to return the current time as a Golang time.Time object
type iClock interface {
	nowUTC() time.Time
}

// realClock implements the iClock interface and returns
// actual current local time in UTC as Golang time.Time object
type realClock struct{}

func (realClock) nowUTC() time.Time {
	return time.Now().UTC()
}

// determineCurrentTimes is a helper function used by DetermineCurrentTimes
// that does the same thing but requires an extra iClock passed in to help
// it dtermine local current time in UTC
func determineCurrentTimes(dal dal.IDal, clock iClock, data string) (string, error) {

	requestedZones := codec.DecodeZoneList(data)
	offsets, err := dal.GetValues(requestedZones)
	if err != nil {
		return "", err
	}

	// Get current time in UTC only once so that
	// current times in all zones we calculate coincide
	currentTime := clock.nowUTC()

	allIso8601times := make([]string, len(offsets))

	for i, offset := range offsets {
		iso8601time := timeutils.ISO8061TimeUsingOffset(currentTime, offset)
		allIso8601times[i] = iso8601time
	}

	return codec.EncodeReadResp(allIso8601times), nil

}

// DetermineCurrentTimes takes the ;-separated list of
// zones coming from a read request and determines the
// current time (in ISO-8061 format) in those zones,
// again as a ;-separated list
func DetermineCurrentTimes(dal dal.IDal, data string) (string, error) {
	encodedCurrTimes, err := determineCurrentTimes(dal, realClock{}, data)
	return encodedCurrTimes, err
}
