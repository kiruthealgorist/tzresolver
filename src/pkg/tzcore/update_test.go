package tzcore

import (
	"testing"
	"tzresolver/src/pkg/dal"
)

/* -- Unit tests (use mocked in memory map DAL) -- */

func TestUpdateZoneOffsets(t *testing.T) {
	// Initialize a new fake/mock dal
	fDal := initFakeDal()

	type args struct {
		dal  dal.IDal
		data string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Simulate adding a zone without error",
			args: args{
				dal:  fDal,
				data: "NewZone,-3600", // new zone, 3600 behind UTC
			},
			wantErr: false, // key point of test is no error
		},
		{
			name: "Simulate updating a (default) zone without error",
			args: args{
				dal:  fDal,
				data: "America/Toronto,0", // now Toronto aligns with UTC
			},
			wantErr: false, // no error!
		},
		{
			name: "Simulate failure in decoding update data (first err type in function)",
			args: args{
				dal:  fDal,
				data: "someZone,-12BADOFFSET",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := UpdateZoneOffsets(tt.args.dal, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("UpdateZoneOffsets() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

/* --- Perf benchmark tests (use real badger DB DAL) --- */

type benchmarkUpdateTestCase struct {
	name string // name of benchmark test case
	data string // zone,off list to update/insert
}

// BenchmarkUpdateSerial tests updating single,double,triple,four zones per request
// in a serial fashion (i.e., 1 client)
func BenchmarkUpdateSerial(b *testing.B) {
	badgerDal := initializeRealBadgerDal("/tmp/badger-bench-serial-updates", b)
	defer dropAndCloseBadgerDal(badgerDal, b)

	// Just do updates on defaults zones with correct offsets that came as default
	tests := []benchmarkUpdateTestCase{
		{"Single zone update", "Etc/UTC,0"},
		{"Double zone update", "America/Toronto,-14400;Etc/UTC,0"},
		{"Three zone update", "America/Toronto,-14400;Europe/Berlin,3600;Etc/UTC,0"},
		{"Four zone update", "America/Toronto,-14400;Europe/Berlin,3600;Etc/UTC,0;Asia/Dubai,14400"},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				err := UpdateZoneOffsets(badgerDal, tt.data)
				if err != nil {
					b.Fatal(err)
				}
			}
		})
	}
}

// BenchmarkUpdateParallel tests deleting single,double,triple,four zones per request
// in a parallel fashion (i.e., multiple clients, look for the -N in benchmark output
// which denotes the number of concurrent clients)
func BenchmarkUpdateParallel(b *testing.B) {
	badgerDal := initializeRealBadgerDal("/tmp/badger-bench-parallel-updates", b)
	defer dropAndCloseBadgerDal(badgerDal, b)

	// Just do updates on defaults zones with correct offsets that came as default
	tests := []benchmarkUpdateTestCase{
		{"Single zone update", "Etc/UTC,0"},
		{"Double zone update", "America/Toronto,-14400;Etc/UTC,0"},
		{"Three zone update", "America/Toronto,-14400;Europe/Berlin,3600;Etc/UTC,0"},
		{"Four zone update", "America/Toronto,-14400;Europe/Berlin,3600;Etc/UTC,0;Asia/Dubai,14400"},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					err := UpdateZoneOffsets(badgerDal, tt.data)
					if err != nil {
						b.Fatal(err)
					}
				}
			})
		})
	}
}
