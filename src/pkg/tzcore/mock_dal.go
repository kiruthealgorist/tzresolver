package tzcore

import (
	"fmt"
	"sync"
)

// fakeDal implements to dal.IDal interface from the dal package.
// This allows us to mock the unit tests herein without going through a real
// dal and accessing a real database.
type fakeDal struct {
	// We will use an in memory hash to mimic a DB
	dbMap map[string]int
	// To make it thread-safe just in case
	lock sync.RWMutex
}

// initFakeDal creates the mock hash based DAL
// with zones that mimic the ones used by overall program
func initFakeDal() *fakeDal {
	dbMap := make(map[string]int)
	dbMap["America/Toronto"] = -14400
	dbMap["Europe/Berlin"] = 3600
	dbMap["Etc/UTC"] = 0
	dbMap["Asia/Dubai"] = 14400

	return &fakeDal{
		dbMap: dbMap,
		lock:  sync.RWMutex{},
	}
}

// Close closes the mock db
func (fDal *fakeDal) Close() error {
	// Nothing to do
	return nil
}

// GetValues gets values from mock db
func (fDal *fakeDal) GetValues(keys []string) ([]int, error) {

	// Values we will ultimately return
	valuesRet := make([]int, len(keys))

	// Read lock hash
	fDal.lock.RLock()
	defer fDal.lock.RUnlock()

	for i, key := range keys {
		value, exists := fDal.dbMap[key]
		if !exists {
			// Contract for GetValues for any DAL is that a missing key returns error
			return nil, fmt.Errorf("Key <%v> does not exist", key)
		}
		valuesRet[i] = value
	}

	return valuesRet, nil
}

// UpdateKeyValues updates keys/values into mock db
func (fDal *fakeDal) UpdateKeyValues(keyValueMap map[string]int) error {

	// Write lock hash db map
	fDal.lock.Lock()
	defer fDal.lock.Unlock()

	for key, value := range keyValueMap {
		fDal.dbMap[key] = value
	}

	return nil
}

// DeleteKeys deletes keys from mock db
func (fDal *fakeDal) DeleteKeys(keys []string) error {

	// Write lock hash db map
	fDal.lock.Lock()
	defer fDal.lock.Unlock()

	for _, key := range keys {
		if _, exists := fDal.dbMap[key]; exists {
			// Delete entry from db map if it exists
			delete(fDal.dbMap, key)
		}
	}

	return nil
}

// DropAll deletes all keys from mock db
func (fDal *fakeDal) DropAll() error {

	// Write lock hash db map
	fDal.lock.Lock()
	defer fDal.lock.Unlock()

	// Set to new map to simulate clearing
	fDal.dbMap = make(map[string]int)

	return nil
}
