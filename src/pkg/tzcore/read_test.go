package tzcore

import (
	"testing"
	"time"
	"tzresolver/src/pkg/dal"
)

// fakeClock implements the iClock interface and returns
// a mocked response for current local time in UTC
// that is always equal to 2009-11-10T23:00:00
// (Because we don't want "current time" to vary each time we run tests)
type fakeClock struct{}

func (fakeClock) nowUTC() time.Time {
	return time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
}

/* --- Unit tests (using a mocked in-memory hash db DAL) -- */

func Test_determineCurrentTimes(t *testing.T) {
	// Initialize a new fake/mock dal
	fDal := initFakeDal()

	type args struct {
		dal   dal.IDal
		clock iClock
		data  string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Get current local time in UTC zone",
			args: args{
				dal:   fDal,
				clock: fakeClock{},
				data:  "Etc/UTC", // Is a default zone (0 offset)
			},
			want:    "2009-11-10T23:00:00",
			wantErr: false,
		},
		{
			name: "Get current local time in non-UTC zone (America/Toronto)",
			args: args{
				dal:   fDal,
				clock: fakeClock{},
				data:  "America/Toronto",
			},
			want:    "2009-11-10T19:00:00", // 4 hours behind UTC in above sub-case
			wantErr: false,
		},
		{
			name: "Get current local time for multiple zones",
			args: args{
				dal:   fDal,
				clock: fakeClock{},
				data:  "America/Toronto;Etc/UTC",
			},
			want:    "2009-11-10T19:00:00;2009-11-10T23:00:00",
			wantErr: false,
		},
		{
			name: "Test failure in determining local times",
			args: args{
				dal:   fDal,
				clock: fakeClock{},
				data:  "non-existing-zone-should-do-job",
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := determineCurrentTimes(tt.args.dal, tt.args.clock, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("determineCurrentTimes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("determineCurrentTimes() = %v, want %v", got, tt.want)
			}
		})
	}
}

/* --- Perf benchmark tests (using real badger DB DAL) --- */

type benchmarkReadTestCase struct {
	name string // name of benchmark test case
	data string // zone list for the read
}

// BenchmarkReadSerial tests reading single,double,triple,four zones per request
// in serial fashion (i.e., 1 client)
func BenchmarkReadSerial(b *testing.B) {
	badgerDal := initializeRealBadgerDal("/tmp/badger-bench-serial-reads", b)
	defer dropAndCloseBadgerDal(badgerDal, b)

	tests := []benchmarkReadTestCase{
		{"Single zone reads", "Etc/UTC"},
		{"Double zone reads", "America/Toronto;Etc/UTC"},
		{"Three zone reads", "America/Toronto;Europe/Berlin;Etc/UTC"},
		{"Four zone reads", "America/Toronto;Europe/Berlin;Etc/UTC;Asia/Dubai"},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				_, err := DetermineCurrentTimes(badgerDal, tt.data)
				if err != nil {
					b.Fatal(err)
				}
			}
		})
	}
}

// BenchmarkReadParallel tests reading single,double,triple,four zones per request
// in a parallel fashion (i.e., multiple clients, look for the -N in benchmark output
// which denotes the number of concurrent clients)
func BenchmarkReadParallel(b *testing.B) {
	badgerDal := initializeRealBadgerDal("/tmp/badger-bench-parallel-reads", b)
	defer dropAndCloseBadgerDal(badgerDal, b)

	tests := []benchmarkReadTestCase{
		{"Single zone reads", "Etc/UTC"},
		{"Double zone reads", "America/Toronto;Etc/UTC"},
		{"Three zone reads", "America/Toronto;Europe/Berlin;Etc/UTC"},
		{"Four zone reads", "America/Toronto;Europe/Berlin;Etc/UTC;Asia/Dubai"},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					_, err := DetermineCurrentTimes(badgerDal, tt.data)
					if err != nil {
						b.Fatal(err)
					}
				}
			})
		})
	}
}
