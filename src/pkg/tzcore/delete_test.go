package tzcore

import (
	"testing"
	"tzresolver/src/pkg/dal"
)

/* -- Unit tests (use mocked in memory hash DAL) -- */

func TestDeleteZones(t *testing.T) {
	// Initialize a new fake/mock dal
	fDal := initFakeDal()

	type args struct {
		dal  dal.IDal
		data string
	}
	tests := []struct {
		dal     dal.IDal
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Simulate deleting a zone without error",
			args: args{
				dal:  fDal,
				data: "America/Toronto",
			},
			wantErr: false, // key point of test is no error
		},
		{
			name: "Simulate deleting multiple zones without error",
			args: args{
				dal:  fDal,
				data: "Etc/UTC;Asia/Dubai",
			},
			wantErr: false, // no error
		},
		{
			name: "Simulate a deletion of non-existing zone (should NOT fail)",
			args: args{
				dal:  fDal,
				data: "Zone/Does-not-exit",
			},
			wantErr: false, // we do NOT error on a delete of non-existing zone
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fDal = initFakeDal()

			if err := DeleteZones(tt.args.dal, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("DeleteZones() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

/* --- Perf benchmark tests (using real badger DB DAL) --- */

type benchmarkDeleteTestCase struct {
	name string // name of benchmark test case
	data string // zone list to delete
}

// BenchmarkDeleteSerial tests deleting single,double,triple,four zones per request
// in a serial fashion (i.e., 1 client)
func BenchmarkDeleteSerial(b *testing.B) {
	badgerDal := initializeRealBadgerDal("/tmp/badger-bench-parallel-deletes", b)
	defer dropAndCloseBadgerDal(badgerDal, b)

	// Just do updates on default zones with correct offsets
	tests := []benchmarkDeleteTestCase{
		{"Single zone delete", "Etc/UTC"},
		{"Double zone delete", "America/Toronto;Etc/UTC"},
		{"Three zone delete", "America/Toronto;Europe/Berlin;Etc/UTC"},
		{"Four zone delete", "America/Toronto;Europe/Berlin;Etc/UTC;Asia/Dubai"},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				err := DeleteZones(badgerDal, tt.data)
				if err != nil {
					b.Fatal(err)
				}
			}
		})
	}
}

// BenchmarkDeleteParallel tests deleting single,double,triple,four zones per request
// in a parallel fashion (i.e., multiple clients, look for the -N in benchmark output
// which denotes the number of concurrent clients)
func BenchmarkDeleteParallel(b *testing.B) {
	badgerDal := initializeRealBadgerDal("/tmp/badger-bench-parallel-deletes", b)
	defer dropAndCloseBadgerDal(badgerDal, b)

	// Just do deletes on defaults zones
	tests := []benchmarkDeleteTestCase{
		{"Single zone delete", "Etc/UTC"},
		{"Double zone delete", "America/Toronto;Etc/UTC"},
		{"Three zone delete", "America/Toronto;Europe/Berlin;Etc/UTC"},
		{"Four zone delete", "America/Toronto;Europe/Berlin;Etc/UTC;Asia/Dubai"},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					err := DeleteZones(badgerDal, tt.data)
					if err != nil {
						b.Fatal(err)
					}
				}
			})
		})
	}
}
