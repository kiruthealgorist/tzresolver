package tzcore

import (
	"testing"
	bdal "tzresolver/src/pkg/dal/badgerdal"
)

/*
   Herein are helper functions to set up a real badgerDB dal for the
   benchmark tests that appear at the bottom of read/update/delete_test.go

   Note: The unit-tests use a mocked dal, but the benchmark tests use
         a real badgerDB with the help of these funtions.
*/

// initializeRealBadgerDal creates and opens a badgerDAL at the dbPath
func initializeRealBadgerDal(dbPath string, b *testing.B) *bdal.BadgerDal {
	badgerDal, err := bdal.InitBadgerDal(dbPath, true, 0)
	if err != nil {
		b.Fatalf("Cannot open badger db at location <%v> with err: %v",
			dbPath, err)
	}

	// Just in case this db at path was lingering from before, nuke all keys
	err = badgerDal.DropAll()
	if err != nil {
		b.Fatal(err)
	}

	// Initialize with some default zones/offsets to help with testing.
	zoneOffsets := make(map[string]int)
	zoneOffsets["America/Toronto"] = -14400
	zoneOffsets["Europe/Berlin"] = 3600
	zoneOffsets["Etc/UTC"] = 0
	zoneOffsets["Asia/Dubai"] = 14400

	err = badgerDal.UpdateKeyValues(zoneOffsets)
	if err != nil {
		b.Fatal(err)
	}
	return badgerDal

}

// dropAndCloseBadgerDal drops all keys and closes data store
func dropAndCloseBadgerDal(badgerDal *bdal.BadgerDal, b *testing.B) {
	badgerDal.DropAll()
	badgerDal.Close()
}
