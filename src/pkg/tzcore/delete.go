package tzcore

import (
	"tzresolver/src/pkg/codec"
	"tzresolver/src/pkg/dal"
)

// DeleteZones takes a ;-separated list of zones
// and deletes those zones from storage state
func DeleteZones(dal dal.IDal, data string) error {

	zonesToDelete := codec.DecodeZoneList(data)

	err := dal.DeleteKeys(zonesToDelete)
	if err != nil {
		return err
	}

	return nil
}
