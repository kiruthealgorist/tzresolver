package tzcore

import (
	"tzresolver/src/pkg/codec"
	"tzresolver/src/pkg/dal"
)

// UpdateZoneOffsets takes a ;-separated list of
// zones,offset pairs and updates or inserts
// data storage state for this zone
func UpdateZoneOffsets(dal dal.IDal, data string) error {

	zoneOffsetsToUpdate, err := codec.DecodeUpdateData(data)
	if err != nil {
		return err
	}

	err = dal.UpdateKeyValues(zoneOffsetsToUpdate)
	if err != nil {
		return err
	}

	return nil
}
