// Package tzcore is the "processing" (e.g. business logic)
// layer of application
// Code in this package will call into the DAL to set or get data
// Higher level code will call into this package to get results
// of business logic (i.e., processing, algorithms)
package tzcore
