package server

import (
	"bufio"
	"log"
	"net"
	"strconv"
	"strings"

	"tzresolver/src/pkg/api"
	"tzresolver/src/pkg/dal"
)

// TCPServer struct is passed through from top-level program/boot code
// with information about connection pooling (size of pool, and connection queue size)
// and also data abstraction layer
type TCPServer struct {
	Dal                     dal.IDal
	ConnectionPoolSize      int
	ConnectionPoolQueueSize int
}

// RunTCPServer runs a TCP server that listens on argument port
// (for all IP addresses)
// Further, for each accepted connection, it funnels it onto a queue
// to be serviced by worker threads that persistently interact with
// clients after the initial connection is made.
func (srv *TCPServer) RunTCPServer(port int) {

	// Bind and listen to receiving port
	addr := "0.0.0.0:" + strconv.Itoa(port)
	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Fatal error, cannot run server on designated port %v\n", port)
	}
	defer l.Close()

	// Create connecton pool queue and pool
	connsQueue := make(chan net.Conn, srv.ConnectionPoolQueueSize)
	for threadID := 1; threadID <= srv.ConnectionPoolQueueSize; threadID++ {
		go srv.handleConnections(connsQueue)
	}

	// For every accepted connection, offload connection
	// to connection pool of workers (via queue)
	for {
		conn, err := l.Accept()
		if err != nil {
			// If we cannot open a reverse connection to client from
			// an ephemeral TCP port, just log it but do not error
			// as this could be a temporary error.
			// TODO: Too many of these errors should trigger a failing health check
			//       that something like kubernetes can trigger a restart of
			//       program with
			log.Printf("Cannot open connection to server client with err: %v\n", err)
			continue
		}
		// enqueue connection
		connsQueue <- conn
	}
}

// handleConnections is intended to run as a goroutine/lightweight-thread
// that continuously pulls connections off the connections queue (connsQueue)
func (srv *TCPServer) handleConnections(connsQueue <-chan net.Conn) {
	// Create a API handler struct used to service API requests
	hdlr := api.Handler{
		Dal: srv.Dal,
	}

	// Continuously pull connections from connections queue
	for conn := range connsQueue {

		// Update TCP connection into in API handler and open a reader for connection
		hdlr.TCPconn = conn

		// Continuously read commands line by line
		reader := bufio.NewReader(conn)
		for {
			reqLine, err := reader.ReadString('\n')
			if err != nil {
				// If reading of next line failed, drop TCP connection
				conn.Close()
				break
			}

			// Only consider non-empty command lines
			reqLine = strings.TrimSuffix(reqLine, "\n")
			if len(reqLine) > 0 {
				err := hdlr.ServiceRequest(reqLine)
				if err != nil {
					// If request failed, we drop the TCP connection
					conn.Close()
					break
				}
			}
		}
		conn.Close()
	}
}
