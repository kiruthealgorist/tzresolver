package api

import (
	"testing"
)

func Test_determineReqTypeData(t *testing.T) {
	type args struct {
		req string
	}
	tests := []struct {
		name    string
		args    args
		reqType int
		data    string
		wantErr bool
	}{
		{
			name: "Read type request",
			args: args{
				req: "R Foo;Bar",
			},
			reqType: READ,
			data:    "Foo;Bar",
			wantErr: false,
		},
		{
			name: "Update type request",
			args: args{
				req: "U Bar;Foo",
			},
			reqType: UPDATE,
			data:    "Bar;Foo",
			wantErr: false,
		},
		{
			name: "Delete type request",
			args: args{
				req: "D Foo/Bar;Electric/Car",
			},
			reqType: DELETE,
			data:    "Foo/Bar;Electric/Car",
			wantErr: false,
		},
		{
			name: "Invalid/Unknown request",
			args: args{
				req: "X ThisXCharIsInvalid",
			},
			reqType: 0,
			data:    "",
			wantErr: true,
		},
		{
			// For performance sake, after the R,U,D first character specifying
			// the request type, we really do not check that 2nd character is a space.
			// Here we test that 2nd character can be anything.
			name: "Second character in request is not space",
			args: args{
				req: "R|Foo/Bar",
			},
			reqType: READ,
			data:    "Foo/Bar",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actReqType, actData, err := determineReqTypeData(tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("determineReqTypeData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if actReqType != tt.reqType {
				t.Errorf("determineReqTypeData() actReqType = %v, reqType %v", actReqType, tt.reqType)
			}
			if actData != tt.data {
				t.Errorf("determineReqTypeData() actData = %v, data %v", actData, tt.data)
			}
		})
	}
}

func Test_routeRequest(t *testing.T) {
	// Create dummy API handler struct (field values not important)
	hdlr := Handler{}

	type args struct {
		reqType int
		data    string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := hdlr.routeRequest(tt.args.reqType, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("routeRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
