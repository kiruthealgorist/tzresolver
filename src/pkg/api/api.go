package api

import (
	"fmt"
	"net"
	"tzresolver/src/pkg/dal"
	"tzresolver/src/pkg/tzcore"
)

// Enum constants to denote command type
const (
	READ = iota + 1
	UPDATE
	DELETE
)

// Handler holds connectionn necessary for API handler code to
// connect both towards the TCP connection and towards DAL
type Handler struct {
	TCPconn net.Conn // TCP connection facing the client
	Dal     dal.IDal // data abstraction layer facing data store
}

// determineReqTypeDate determines command type
func determineReqTypeData(req string) (int, string, error) {

	firstChar := req[0]
	data := req[2:] // skip the 2nd character (typically a space)

	var reqType int
	if firstChar == 'R' {
		reqType = READ
	} else if firstChar == 'U' {
		reqType = UPDATE
	} else if firstChar == 'D' {
		reqType = DELETE
	} else {
		return 0, "", fmt.Errorf("Bad request type")
	}

	return reqType, data, nil
}

// readHandler services R/read commands
func (hdlr *Handler) readHandler(data string) error {
	resp, err := tzcore.DetermineCurrentTimes(hdlr.Dal, data)
	if err != nil {
		return err
	}

	// Respond to client with time list respone
	_, err = fmt.Fprintf(hdlr.TCPconn, "%s\n", resp)
	return err
}

// updateHandler services U/update commands
func (hdlr *Handler) updateHandler(data string) error {

	return tzcore.UpdateZoneOffsets(hdlr.Dal, data)
}

// deleteHandler services D/delete commands
func (hdlr *Handler) deleteHandler(data string) error {

	return tzcore.DeleteZones(hdlr.Dal, data)
}

// routeRequest routes requests to appropriate read/update/delete handler
func (hdlr *Handler) routeRequest(reqType int, data string) error {
	var err error

	if reqType == READ {
		err = hdlr.readHandler(data)
	} else if reqType == UPDATE {
		err = hdlr.updateHandler(data)
	} else if reqType == DELETE {
		err = hdlr.deleteHandler(data)
	}

	return err
}

// ServiceRequest is called by tcp server layer
// with the client command line to be serviced
func (hdlr *Handler) ServiceRequest(req string) error {

	reqType, data, err := determineReqTypeData(req)
	if err != nil {
		return err
	}
	return hdlr.routeRequest(reqType, data)
}
