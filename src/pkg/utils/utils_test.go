package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetKeysValuesStrIntMap(t *testing.T) {
	type args struct {
		theMap map[string]int
	}
	tests := []struct {
		name   string
		args   args
		keys   []string
		values []int
	}{
		{
			name: "nil map",
			args: args{
				theMap: nil,
			},
			keys:   []string{},
			values: []int{},
		},
		{
			name: "Empty map",
			args: args{
				theMap: map[string]int{},
			},
			keys:   []string{},
			values: []int{},
		},
		{
			name: "Non-empty map",
			args: args{
				theMap: map[string]int{
					"Foo": 1,
					"Bar": -10,
					"Car": 0,
				},
			},
			keys:   []string{"Foo", "Bar", "Car"},
			values: []int{1, -10, 0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actualKeys, actualValues := GetKeysValuesFromMap(tt.args.theMap)

			// Check that keys, values match expected
			assert.ElementsMatch(t, actualKeys, tt.keys)
			assert.ElementsMatch(t, actualValues, tt.values)
		})
	}
}
