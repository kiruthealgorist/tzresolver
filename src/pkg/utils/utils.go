package utils

// GetKeysValuesFromMap returns the string keys and integer values
// of a string->int map as slices
func GetKeysValuesFromMap(theMap map[string]int) ([]string, []int) {
	keys := make([]string, len(theMap))
	values := make([]int, len(theMap))

	i := 0
	for k, v := range theMap {
		keys[i] = k
		values[i] = v
		i++
	}
	return keys, values
}
