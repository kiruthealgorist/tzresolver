package dal

// IDal interface is describes any abstract data layer.
// That is, any data abstraction layer (for a specific kv store) implemented
// as a subpackage of this one (e.g. badgerdal) must implement this interface.
type IDal interface {
	// Gets int values associated with argument string values
	// Will return error with no values returned if any key is missing
	GetValues([]string) ([]int, error)
	// Updates db with the keys/values (either a replace or insert)
	UpdateKeyValues(map[string]int) error
	// Deletes keys from db (if any key does not exist, that is skipped without error)
	DeleteKeys(keys []string) error
	// Deletes all keys form db
	DropAll() error
	// Closes db
	Close() error
}
