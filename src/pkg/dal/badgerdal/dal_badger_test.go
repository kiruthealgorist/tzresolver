package dalbadger

import (
	"log"
	"os"
	"reflect"
	"testing"

	"github.com/dgraph-io/badger"

	"tzresolver/src/pkg/utils"
)

// bDal is a DAL interfacing with BadgerDB
// Global variable by design to make it
// easier to share between unit-tests
var bDal BadgerDal

func setupTestSuite() error {
	db, err := badger.Open(badger.DefaultOptions("/tmp/badger-unit-test"))
	if err != nil {
		log.Fatal(err)
	}
	bDal.db = db

	return setupInitialDefaultZones()
}

func setupTestCase() error {
	bDal.db.DropAll()
	setupInitialDefaultZones()

	return nil
}

func setupInitialDefaultZones() error {
	initialZoneOffsets := make(map[string]int)

	initialZoneOffsets["America/Toronto"] = -14400
	initialZoneOffsets["Europe/Berlin"] = 3600
	initialZoneOffsets["Etc/UTC"] = 0
	initialZoneOffsets["Asia/Dubai"] = 14400

	return bDal.UpdateKeyValues(initialZoneOffsets)
}

func shutdownTestSuite() {
	bDal.db.DropAll()
	bDal.db.Close()
}

func TestMain(m *testing.M) {
	err := setupTestSuite()
	if err != nil {
		os.Exit(1)
	}
	exitCode := m.Run()
	shutdownTestSuite()
	os.Exit(exitCode)
}

func TestBadgerDal_GetValues(t *testing.T) {
	setupTestCase()
	type args struct {
		keys []string
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{
			name: "Single Read",
			args: args{
				keys: []string{"America/Toronto"},
			},
			want:    []int{-14400}, // read must return offset for Toronto
			wantErr: false,
		},
		{
			name: "Two reads in single GetValues transaction",
			args: args{
				keys: []string{"America/Toronto", "Etc/UTC"},
			},
			want:    []int{-14400, 0}, // read must return both offsets
			wantErr: false,
		},
		{
			name: "Three reads in single GetValues transaction, one of them read twice",
			args: args{
				keys: []string{"America/Toronto", "Etc/UTC", "America/Toronto"},
			},
			want:    []int{-14400, 0, -14400},
			wantErr: false,
		},
		{
			name: "Two reads, second one fails, must return nothing and only error",
			args: args{
				keys: []string{"America/Toronto", "Non-existing-key"}, // 2nd key does not exist
			},
			want:    nil,  // must return nothing
			wantErr: true, // but we must have error returned
		},
		{
			// Note: This sub-case also implicity makes sure that previous txn borking
			//       does not leave db in an bad state, so this read must succeed
			//       (even when we use a key that was attempted to be read from previous sub-case)
			name: "Three reads in single GetValues transaction (including a key used in above failed transction)",
			args: args{
				keys: []string{"America/Toronto", "Etc/UTC", "Asia/Dubai"},
			},
			want:    []int{-14400, 0, 14400},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := bDal.GetValues(tt.args.keys)
			if (err != nil) != tt.wantErr {
				t.Errorf("BadgerDal.GetValues() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BadgerDal.GetValues() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBadgerDal_UpdateKeyValues(t *testing.T) {
	setupTestCase()
	type args struct {
		keyValueMap map[string]int
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Add new key-value",
			args: args{
				keyValueMap: map[string]int{
					"NewZone": -100,
				},
			},
			wantErr: false,
		},
		{
			name: "Update a key-value to existinv value", // update a key already in db from start of case
			args: args{
				keyValueMap: map[string]int{
					"Etc/UTC": 0, // this update will keep value same
				},
			},
			wantErr: false,
		},
		{
			name: "Update a key-value to new value", // update a key we set up at start of case
			args: args{
				keyValueMap: map[string]int{
					"America/Toronto": 0, // this key was set up initially with value -14400
				},
			},
			wantErr: false,
		},
		{
			name: "Add key and update another same time",
			args: args{
				keyValueMap: map[string]int{
					"Brand-new-key": -100,
					"Asia/Dubai":    99, // key we set up at start of case (had value +14400)
				},
			},
			wantErr: false,
		},
		// Note: There is no sub-case to test the atomicity of partial failures
		//       because inducing a failure on updates to kv store is not trivial
		//       We rely on Badger's more "internal" unit tests and their Jepsen style
		//       tests to give us confidence in this regard.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// First make sure update does not error
			if err := bDal.UpdateKeyValues(tt.args.keyValueMap); (err != nil) != tt.wantErr {
				t.Errorf("BadgerDal.UpdateKeyValues() error = %v, wantErr %v", err, tt.wantErr)
			}

			// Check that values updates/added exist in db with correct value
			// Note: This implicitly uses another DAL function to check that correct
			//       values exist in db
			keys, expectedValues := utils.GetKeysValuesFromMap(tt.args.keyValueMap)
			values, err := bDal.GetValues(keys)
			if err != nil {
				t.Errorf("BadgerDal.UpdateKeysValues(), error while querying database for expected keys, err: %v", err)
			}
			if !reflect.DeepEqual(values, expectedValues) {
				t.Errorf("BadgerDal.UpdateKeysValues(), expected values to be in DB=%+v, only found=%+v", expectedValues, values)
			}
		})
	}
}

func TestBadgerDal_DeleteKeys(t *testing.T) {
	setupTestCase()
	type args struct {
		keys []string
	}
	tests := []struct {
		name                string
		args                args
		wantErr             bool
		keysShouldBeDeleted []string // keys we know for should be deleted after a sub-case
	}{
		{
			name: "Try to delete a non-existing key",
			args: args{
				keys: []string{"A-key-not-existing!"},
			},
			wantErr: false, // does not error! By design badger does not error and rollback the transaction.
		},
		{
			name: "Delete existing key", // default key we added at start of test case
			args: args{
				keys: []string{"Asia/Dubai"},
			},
			wantErr: false,
		},
		{
			name: "Delete and existing key in same txn as deleting a non-existing",
			args: args{
				keys: []string{"Etc/UTC", "Non-existing"},
			},
			wantErr: false,
		},
		// Note: There is no sub-case to test the atomicity of partial failures
		//       because inducing a failure on updates to kv store is not trivial
		//       We rely on Badger's more "internal" unit tests and their Jepsen style
		//       tests to give us confidence in this regard.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := bDal.DeleteKeys(tt.args.keys); (err != nil) != tt.wantErr {
				t.Errorf("BadgerDal.DeleteKeys() error = %v, wantErr %v", err, tt.wantErr)
			}

			// Check that deleted values do not exist in db
			// Check that deleted values do not exist in db
			vals, err := bDal.GetValues(tt.keysShouldBeDeleted)
			if err != nil {
				t.Errorf("Could not run GetValues() after sub-case to determine if keys <%+v> were deleted",
					tt.keysShouldBeDeleted)
			}
			if len(vals) > 0 {
				t.Errorf("The values <%+v> associated with keys <%+v> should have been deleted but were not",
					vals, tt.keysShouldBeDeleted)
			}
		})
	}
}
