package dalbadger

import (
	"fmt"
	"strconv"

	"github.com/dgraph-io/badger"
)

// BadgerDal implements the dal.IDAL interface
type BadgerDal struct {
	db *badger.DB
}

// DropAll drops all the keys in DB
func (bDal *BadgerDal) DropAll() error {
	return bDal.db.DropAll()
}

// Close closes db
func (bDal *BadgerDal) Close() error {
	return bDal.db.Close()
}

// GetValues returns the values as int slice associated with
// the argument string slice of keys
// Note: On any missing key requested or a read error,
//       no values are return, but an error is returned.
func (bDal *BadgerDal) GetValues(keys []string) ([]int, error) {

	keysBytesSlice := make([][]byte, len(keys))
	valuesBytesSlice := make([][]byte, len(keys))

	// Values to be returned
	valuesIntSlice := make([]int, len(keys))

	// Create a bytes slice of keys from string slice
	// TODO: Move to utils package
	for i, key := range keys {
		keysBytesSlice[i] = []byte(key)
	}

	// Open read-only transaction to do reads
	err := bDal.db.View(func(txn *badger.Txn) error {
		for i, keyBytes := range keysBytesSlice {
			valueBytes, err := txn.Get(keyBytes)
			if err != nil {
				// Abort read transaction on any one key read error
				return err
			}
			valuesBytesSlice[i], err = valueBytes.ValueCopy(nil)
			if err != nil {
				// Abort read txn if value cannot be copied
				// from value log
				return err
			}
		}
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("Read transaction failed due to : %v", err)
	}

	// Convert returned bytes slice of values into into
	for i, valueBytes := range valuesBytesSlice {
		valueStr := string(valueBytes)
		valueInt, err := strconv.Atoi(valueStr)
		if err != nil {
			// This should never happen; unless someone manually injected a bad value that isn't an int
			return nil, fmt.Errorf("Read transaction for key <%v> read value <%v> which is not an int",
				keys[i], valueStr)
		}
		valuesIntSlice[i] = valueInt
	}

	return valuesIntSlice, nil
}

// UpdateKeyValues updates the db with keys and associated values from argument map.
// Update here refers to an insert if necessary, or a replacement.
func (bDal *BadgerDal) UpdateKeyValues(keyValueMap map[string]int) error {

	valuesBytesSlice := make([][]byte, len(keyValueMap))
	keysBytesSlice := make([][]byte, len(keyValueMap))

	// Convert keys and values slices into slices of bytes
	// TODO: Move to utils package
	i := 0
	for key, value := range keyValueMap {
		keysBytesSlice[i] = []byte(key)
		valuesBytesSlice[i] = []byte(strconv.Itoa(value))
		i++
	}

	// Open read-write transaction to do updates/inserts
	err := bDal.db.Update(func(txn *badger.Txn) error {
		for i, keyBytes := range keysBytesSlice {
			valueBytes := valuesBytesSlice[i]
			err := txn.Set(keyBytes, valueBytes)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("Update transaction failed with err: %v", err)
	}
	return nil
}

// DeleteKeys deletes keys from argument from db.
// If a key does not exist amongst those provided, there will be NO error
// and any other key that does exist will be deleted successfully.
func (bDal *BadgerDal) DeleteKeys(keys []string) error {

	keysBytesSlice := make([][]byte, len(keys))

	// Create a bytes slice of keys from string slice
	// TODO: Move to utils package
	for i, key := range keys {
		keysBytesSlice[i] = []byte(key)
	}

	// Open read/write transaction to do the deletes
	err := bDal.db.Update(func(txn *badger.Txn) error {
		for _, keyBytes := range keysBytesSlice {
			err := txn.Delete(keyBytes)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("Delete transaction failed with err: %v", err)
	}
	return nil
}
