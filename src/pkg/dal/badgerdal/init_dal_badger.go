package dalbadger

import "tzresolver/src/pkg/db/badgerdb"

// InitBadgerDal returns a data abstraction layer struct that implements the IDal
// interface defined in parent package of this one.
// See documentation on the InitBadgerDB in the badgerdb package for more details
// on the actual arguments to this function which are passed through.
func InitBadgerDal(dbFilePath string, syncWrites bool, tableLoadingMode int) (*BadgerDal, error) {

	// Initialize/open an embedded Badger kv database in read/write mode
	db, err := badgerdb.InitBadgerDB(dbFilePath, syncWrites, tableLoadingMode)
	if err != nil {
		return nil, err
	}
	badgerDal := &BadgerDal{
		db: db,
	}
	return badgerDal, nil
}
