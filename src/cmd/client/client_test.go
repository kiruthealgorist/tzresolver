package main

import "testing"

func Test_ComponentTests(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip tests in short mode - only want to run in a special component test scenario.")
	}
	type args struct {
		commands []string
	}
	tests := []struct {
		name          string
		args          args
		readResponses int
		timeResponses int
	}{
		// Positive regression cases
		// We actualect all the commands to execute, therefore
		// we actualect all the read requests to successful complete
		// (and we always went with a read request to ensure
		//  server did not close TCP connect before that last read)
		{
			name: "Single read of default zone",
			args: args{
				commands: []string{"R Etc/UTC"},
			},
			readResponses: 1,
			timeResponses: 1,
		},
		{
			name: "Single read of multiple default zones",
			args: args{
				commands: []string{"R America/Toronto;Etc/UTC"},
			},
			readResponses: 1,
			timeResponses: 2,
		},
		{
			name: "Add a zone and then read it",
			args: args{
				commands: []string{
					"U Foo/Bar,-100",
					"R Foo/Bar"},
			},
			readResponses: 1,
			timeResponses: 1,
		},
		{
			name: "Add a zone, read it, delete it, read default zone",
			args: args{
				commands: []string{
					"U Foo/Bar,100",
					"R Foo/Bar",
					"D Foo/Bar",
					"R Etc/UTC",
				},
			},
			readResponses: 2,
			timeResponses: 2, // make sure last read succeeded too
		},
		{
			name: "Add multiple zones then read them along with default zone",
			args: args{
				commands: []string{
					"U Foo/Bar,50;AnotherZone,100",
					"R Foo/Bar;AnotherZone;Etc/UTC"},
			},
			readResponses: 1,
			timeResponses: 3,
		},
		// Some negative test cases
		// Here we end the command list with a read but we actualect server to have
		// closed the TCP connect when an earlier error occurred.
		// In end readResponses/timeResponses accounting has to match early closing of TCP conn.
		{
			name: "Attempting to read non-existing zone closes TCP conn immediately",
			args: args{
				commands: []string{
					"R Etc/UTC;non-existing-zone", // 2nd zone does not exist
					"R Etc/UTC"},                  // we should never get to valid read here
			},
			readResponses: 0,
			timeResponses: 0,
		},
		{
			name: "Attempting to delete non-existing zone closes TCP conn immediately",
			args: args{
				commands: []string{
					"D Etc/UTC;non-existing-zone;America/Toronto", // 2nd zone does not exist
					"R America/Toronto",                           // we should never get to valid read here
				},
			},
			readResponses: 0,
			timeResponses: 0,
		},
		// Note: Point of this case is to make sure the 2 keys in above sub-case
		//       were actually deleted, even though one of the keys in that batch delete did not exist.
		{
			name: "Check that batch delete with one key not existing still deletes others successfull",
			args: args{
				commands: []string{"R Etc/UTC;America/Toronto"}, // Should be deleted!
			},
			readResponses: 0,
			timeResponses: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actualReadResponses, actualTimeResponses := runClient(tt.args.commands)
			if actualReadResponses != tt.readResponses {
				t.Errorf("runClient() actualReadResponses = %v, readResponses %v", actualReadResponses, tt.readResponses)
			}
			if actualTimeResponses != tt.timeResponses {
				t.Errorf("runClient() actualTimeResponses = %v, readResponses %v", actualTimeResponses, tt.timeResponses)
			}
		})
	}
}
