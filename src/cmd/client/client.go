package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func runClient(commands []string) (int, int) {
	addr := "127.0.0.1:6666"
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		fmt.Println(err)
		os.Exit(100)
	}
	defer conn.Close()

	reader := bufio.NewReader(conn)
	readResponses := 0
	timeResponses := 0
	for _, cmd := range commands {
		// send command to socket
		fmt.Printf("Sending command: %s\n", cmd)
		fmt.Fprintf(conn, "%s\n", cmd)

		// listen for reply if command was a read request
		if cmd[0] == 'R' {
			message, err := reader.ReadString('\n')
			if err != nil {
				break
			}
			fmt.Println("Message from server: " + message)

			readResponses++
			timeResponses += len(strings.Split(message, ";"))
		}
	}

	return readResponses, timeResponses
}

func main() {
	commands := []string{
		"R America/Toronto;Etc/UTC",
		"U Wakanda,0;DisneyLand,-100",
		"R Wakanda;DisneyLand;America/Toronto",
		"D Wakanda",
		"R Wakanda",
		"R DisneyLand;Etc/UTC",
	}

	runClient(commands)
}
