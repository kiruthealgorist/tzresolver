package main

import (
	"log"
	"os"
	"runtime"
	"strconv"
	"syscall"

	"tzresolver/src/pkg/dal"
	bdal "tzresolver/src/pkg/dal/badgerdal"
	"tzresolver/src/pkg/server"
)

// If PORT env var not provided, application listens on this port
const defaultPort = 6666

// getPort returns port the program should listen on
// Note: Currently the IP 0.0.0.0 bound to, so will accept
//       connections from any IP.
func getPort() int {
	// Get PORT from environment
	portEnv := os.Getenv("PORT")
	if portEnv == "" {
		return defaultPort
	}

	port, err := strconv.Atoi(portEnv)
	if err != nil {
		log.Fatalf("Env. var. PORT's value <%v> is not an integer", portEnv)
	}
	return port
}

// maximizeFileDescLimit increases resource limit on number of file descripters
// to maximum allowed for this process
func maximizeFileDescLimit() {
	var rLimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
	rLimit.Cur = rLimit.Max
	if err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}

	log.Printf("Settings limit on open file descriptors to: %d", rLimit.Cur)
}

// isDefaultZonesRequired checks env var INIT_DEFAULT_ZONES is
// set to something
func isDefaultZonesRequired() bool {
	return os.Getenv("INIT_DEFAULT_ZONES") != ""
}

// initializeDefaultZones drops all keys from data store
// and initializes with the default zones/offsets
func initializeDefaultZones(dal dal.IDal) {

	dal.DropAll()

	initialZoneOffsets := make(map[string]int)
	initialZoneOffsets["America/Toronto"] = -14400
	initialZoneOffsets["Europe/Berlin"] = 3600
	initialZoneOffsets["Etc/UTC"] = 0
	initialZoneOffsets["Asia/Dubai"] = 14400

	err := dal.UpdateKeyValues(initialZoneOffsets)
	if err != nil {
		log.Fatalf("Could not clear db and initialize default zones")
	}

	log.Printf("Data store cleared and initialized with these default zone/offsets:")
	i := 1
	for zone, offset := range initialZoneOffsets {
		log.Printf("%v. Zone=%v OffsetFromUtcInSeconds=%v", i, zone, offset)
		i++
	}
	log.Printf("Please start server normally now to start using application.")
}

// main is the entry point to program
func main() {

	// GOMAXPROC controls how many kernel-level threads any goroutines will be multiplexed on.
	// TODO: Benchmark effect of varying this, though some results do suggest this is right setting.
	runtime.GOMAXPROCS(runtime.NumCPU())

	log.Printf("Tzresolver starting.")
	maximizeFileDescLimit()

	// Open a DAL for badger db
	// Note: This block of code can be changed into a wrapper/factor driven by config
	//       if we have multiple DALs to choose from
	dbPath := "/tmp/tzresolver-badger-db" // TODO: Make this a config or env. var.
	badgerDal, err := bdal.InitBadgerDal(dbPath, true, 2)
	if err != nil {
		log.Fatalf("Cannot open badger db at location <%v> with err: %v",
			dbPath, err)
	}

	if isDefaultZonesRequired() {
		initializeDefaultZones(badgerDal)
		// Exit immediately, user has to restart app in regular mode
		os.Exit(0)
	}

	// Create struct for TCP Server layer
	srv := server.TCPServer{
		Dal:                     badgerDal,
		ConnectionPoolSize:      runtime.NumCPU(),
		ConnectionPoolQueueSize: 4 * runtime.NumCPU(),
	}

	port := getPort()
	log.Printf("Tzresolver TCP server starting on port %v\n", port)
	log.Println("Usage: Do a TCP connection to this port and send commands line by line.")
	log.Println("       Each command starts with character R,U or D, followed by a space")
	log.Println("       followed by a list denoting command arguments. Specifically:")
	log.Printf("1. Read current times in various timezones: R zone1;zone2;...;zoneN")
	log.Printf("2. Update/add zone with offset(sec.) from UTC: U zone1,offset1;...;zoneN,offsetN")
	log.Printf("3. Delete a zone from application: D zone1;zone2;...;zoneN")
	log.Printf("Connections are persistent and can be closed by client, or will be closed on any")
	log.Printf("error by this application (e.g. if a read to non-existing zone is executed)")

	// Run TCP server(s)
	srv.RunTCPServer(port)
}
