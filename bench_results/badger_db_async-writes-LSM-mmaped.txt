rm -rf ./build_artifacts/*
go build -o ./build_artifacts/ src/cmd/tzresolver/tzresolver.go
scripts/stop-server.sh	
go test -v -run=XXX -benchmem -bench=Benchmark --benchtime=10000x -cpu 2,4,6,8,16,32 ./...
?   	tzresolver/src/cmd/client	[no test files]
?   	tzresolver/src/cmd/tzresolver	[no test files]
PASS
ok  	tzresolver/src/pkg/api	0.290s
PASS
ok  	tzresolver/src/pkg/codec	0.114s
?   	tzresolver/src/pkg/dal	[no test files]
badger 2021/08/24 08:45:47 INFO: All 0 tables opened in 0s
badger 2021/08/24 08:45:47 INFO: Replaying file id: 0 at offset: 0
badger 2021/08/24 08:45:47 INFO: Replay took: 13.497µs
badger 2021/08/24 08:45:47 DEBUG: Value log discard stats empty
PASS
badger 2021/08/24 08:45:47 INFO: DropAll called. Blocking writes...
badger 2021/08/24 08:45:47 INFO: Writes flushed. Stopping compactions now...
badger 2021/08/24 08:45:47 INFO: Deleted 0 SSTables. Now deleting value logs...
badger 2021/08/24 08:45:47 INFO: Value logs deleted. Creating value log file: 0
badger 2021/08/24 08:45:47 INFO: Deleted 1 value log files. DropAll done.
badger 2021/08/24 08:45:47 INFO: Resuming writes
badger 2021/08/24 08:45:47 INFO: Got compaction priority: {level:0 score:1.73 dropPrefixes:[]}
ok  	tzresolver/src/pkg/dal/badgerdal	0.243s
?   	tzresolver/src/pkg/db/badgerdb	[no test files]
?   	tzresolver/src/pkg/server	[no test files]
PASS
ok  	tzresolver/src/pkg/timeutils	0.110s
goos: darwin
goarch: amd64
pkg: tzresolver/src/pkg/tzcore
cpu: Intel(R) Core(TM) i9-9880H CPU @ 2.30GHz
BenchmarkDeleteSerial
BenchmarkDeleteSerial/Single_zone_delete
BenchmarkDeleteSerial/Single_zone_delete-2          	   10000	    103306 ns/op	    2016 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-4          	   10000	    110745 ns/op	    2031 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-6          	   10000	    108288 ns/op	    2047 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-8          	   10000	    111721 ns/op	    2063 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-16         	   10000	    112755 ns/op	    2063 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-32         	   10000	    112468 ns/op	    2079 B/op	      66 allocs/op
BenchmarkDeleteSerial/Double_zone_delete
BenchmarkDeleteSerial/Double_zone_delete-2          	   10000	    103539 ns/op	    2342 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-4          	   10000	    112811 ns/op	    2352 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-6          	   10000	    113833 ns/op	    2353 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-8          	   10000	    113755 ns/op	    2353 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-16         	   10000	    112034 ns/op	    2401 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-32         	   10000	    117736 ns/op	    2401 B/op	      79 allocs/op
BenchmarkDeleteSerial/Three_zone_delete
BenchmarkDeleteSerial/Three_zone_delete-2           	   10000	    110379 ns/op	    3029 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-4           	   10000	    145429 ns/op	    3047 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-6           	   10000	    127906 ns/op	    3048 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-8           	   10000	    118626 ns/op	    3048 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-16          	   10000	    116970 ns/op	    3048 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-32          	   10000	    119714 ns/op	    3049 B/op	      93 allocs/op
BenchmarkDeleteSerial/Four_zone_delete
BenchmarkDeleteSerial/Four_zone_delete-2            	   10000	    104165 ns/op	    3292 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-4            	   10000	    120744 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-6            	   10000	    118975 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-8            	   10000	    121067 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-16           	   10000	    119726 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-32           	   10000	    120973 ns/op	    3310 B/op	     105 allocs/op
BenchmarkDeleteParallel
BenchmarkDeleteParallel/Single_zone_delete
BenchmarkDeleteParallel/Single_zone_delete-2        	   10000	     82639 ns/op	    1945 B/op	      63 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-4        	   10000	     65738 ns/op	    1832 B/op	      57 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-6        	   10000	     48247 ns/op	    1660 B/op	      51 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-8        	   10000	     36341 ns/op	    1524 B/op	      45 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-16       	   10000	     19233 ns/op	    1422 B/op	      39 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-32       	   10000	     11160 ns/op	    1377 B/op	      35 allocs/op
BenchmarkDeleteParallel/Double_zone_delete
BenchmarkDeleteParallel/Double_zone_delete-2        	   10000	     88581 ns/op	    2252 B/op	      76 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-4        	   10000	     67319 ns/op	    2136 B/op	      69 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-6        	   10000	     49468 ns/op	    2053 B/op	      63 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-8        	   10000	     38049 ns/op	    1989 B/op	      59 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-16       	   10000	     21113 ns/op	    1874 B/op	      52 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-32       	   10000	     12262 ns/op	    1785 B/op	      48 allocs/op
BenchmarkDeleteParallel/Three_zone_delete
BenchmarkDeleteParallel/Three_zone_delete-2         	   10000	     97133 ns/op	    2935 B/op	      90 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-4         	   10000	     69559 ns/op	    2762 B/op	      83 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-6         	   10000	     51422 ns/op	    2607 B/op	      76 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-8         	   10000	     40795 ns/op	    2492 B/op	      73 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-16        	   10000	     22484 ns/op	    2276 B/op	      64 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-32        	   10000	     13254 ns/op	    2261 B/op	      61 allocs/op
BenchmarkDeleteParallel/Four_zone_delete
BenchmarkDeleteParallel/Four_zone_delete-2          	   10000	    100645 ns/op	    3203 B/op	     102 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-4          	   10000	     72937 ns/op	    2996 B/op	      94 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-6          	   10000	     55064 ns/op	    2873 B/op	      89 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-8          	   10000	     43763 ns/op	    2754 B/op	      84 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-16         	   10000	     25065 ns/op	    2640 B/op	      77 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-32         	   10000	     14606 ns/op	    2631 B/op	      73 allocs/op
BenchmarkReadSerial
BenchmarkReadSerial/Single_zone_reads
BenchmarkReadSerial/Single_zone_reads-2             	   10000	      1666 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-4             	   10000	      1495 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-6             	   10000	      1465 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-8             	   10000	      1527 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-16            	   10000	      1351 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-32            	   10000	      1583 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Double_zone_reads
BenchmarkReadSerial/Double_zone_reads-2             	   10000	      2423 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-4             	   10000	      2311 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-6             	   10000	      2360 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-8             	   10000	      2345 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-16            	   10000	      2812 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-32            	   10000	      2252 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Three_zone_reads
BenchmarkReadSerial/Three_zone_reads-2              	   10000	      3431 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-4              	   10000	      2956 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-6              	   10000	      3228 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-8              	   10000	      3055 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-16             	   10000	      3059 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-32             	   10000	      2768 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Four_zone_reads
BenchmarkReadSerial/Four_zone_reads-2               	   10000	      3850 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-4               	   10000	      3725 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-6               	   10000	      3655 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-8               	   10000	      3972 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-16              	   10000	      3524 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-32              	   10000	      3495 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel
BenchmarkReadParallel/Single_zone_reads
BenchmarkReadParallel/Single_zone_reads-2           	   10000	      1228 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-4           	   10000	      1064 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-6           	   10000	      1116 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-8           	   10000	      1028 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-16          	   10000	      1092 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-32          	   10000	      1046 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Double_zone_reads
BenchmarkReadParallel/Double_zone_reads-2           	   10000	      1323 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-4           	   10000	      1441 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-6           	   10000	      1354 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-8           	   10000	      1245 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-16          	   10000	      1187 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-32          	   10000	      1195 ns/op	     985 B/op	      26 allocs/op
BenchmarkReadParallel/Three_zone_reads
BenchmarkReadParallel/Three_zone_reads-2            	   10000	      1581 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-4            	   10000	      1378 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-6            	   10000	      1576 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-8            	   10000	      1405 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-16           	   10000	      1283 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-32           	   10000	      1326 ns/op	    1425 B/op	      36 allocs/op
BenchmarkReadParallel/Four_zone_reads
BenchmarkReadParallel/Four_zone_reads-2             	   10000	      2050 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-4             	   10000	      1405 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-6             	   10000	      1464 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-8             	   10000	      1479 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-16            	   10000	      1320 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-32            	   10000	      1381 ns/op	    1848 B/op	      46 allocs/op
BenchmarkUpdateSerial
BenchmarkUpdateSerial/Single_zone_update
BenchmarkUpdateSerial/Single_zone_update-2          	   10000	     95483 ns/op	    2298 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-4          	   10000	    110088 ns/op	    2348 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-6          	   10000	    112167 ns/op	    2364 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-8          	   10000	    113252 ns/op	    2380 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-16         	   10000	    111852 ns/op	    2380 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-32         	   10000	    115918 ns/op	    2396 B/op	      71 allocs/op
BenchmarkUpdateSerial/Double_zone_update
BenchmarkUpdateSerial/Double_zone_update-2          	   10000	     99467 ns/op	    2708 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-4          	   10000	    112785 ns/op	    2750 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-6          	   10000	    115295 ns/op	    2750 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-8          	   10000	    112475 ns/op	    2751 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-16         	   10000	    112930 ns/op	    2799 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-32         	   10000	    113290 ns/op	    2800 B/op	      87 allocs/op
BenchmarkUpdateSerial/Three_zone_update
BenchmarkUpdateSerial/Three_zone_update-2           	   10000	    101438 ns/op	    3455 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-4           	   10000	    115283 ns/op	    3504 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-6           	   10000	    114925 ns/op	    3504 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-8           	   10000	    117301 ns/op	    3504 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-16          	   10000	    115611 ns/op	    3504 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-32          	   10000	    116925 ns/op	    3505 B/op	     104 allocs/op
BenchmarkUpdateSerial/Four_zone_update
BenchmarkUpdateSerial/Four_zone_update-2            	   10000	    105288 ns/op	    3786 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-4            	   10000	    116400 ns/op	    3826 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-6            	   10000	    118077 ns/op	    3826 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-8            	   10000	    119022 ns/op	    3826 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-16           	   10000	    119559 ns/op	    3829 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-32           	   10000	    120168 ns/op	    3831 B/op	     119 allocs/op
BenchmarkUpdateParallel
BenchmarkUpdateParallel/Single_zone_update
BenchmarkUpdateParallel/Single_zone_update-2        	   10000	     81569 ns/op	    2228 B/op	      67 allocs/op
BenchmarkUpdateParallel/Single_zone_update-4        	   10000	     63994 ns/op	    2085 B/op	      60 allocs/op
BenchmarkUpdateParallel/Single_zone_update-6        	   10000	     48945 ns/op	    1979 B/op	      55 allocs/op
BenchmarkUpdateParallel/Single_zone_update-8        	   10000	     37367 ns/op	    1883 B/op	      51 allocs/op
BenchmarkUpdateParallel/Single_zone_update-16       	   10000	     19502 ns/op	    1774 B/op	      44 allocs/op
BenchmarkUpdateParallel/Single_zone_update-32       	   10000	     11369 ns/op	    1720 B/op	      40 allocs/op
BenchmarkUpdateParallel/Double_zone_update
BenchmarkUpdateParallel/Double_zone_update-2        	   10000	     90261 ns/op	    2619 B/op	      83 allocs/op
BenchmarkUpdateParallel/Double_zone_update-4        	   10000	     71301 ns/op	    2589 B/op	      80 allocs/op
BenchmarkUpdateParallel/Double_zone_update-6        	   10000	     50493 ns/op	    2461 B/op	      72 allocs/op
BenchmarkUpdateParallel/Double_zone_update-8        	   10000	     38957 ns/op	    2410 B/op	      67 allocs/op
BenchmarkUpdateParallel/Double_zone_update-16       	   10000	     21819 ns/op	    2282 B/op	      60 allocs/op
BenchmarkUpdateParallel/Double_zone_update-32       	   10000	     12393 ns/op	    2232 B/op	      56 allocs/op
BenchmarkUpdateParallel/Three_zone_update
BenchmarkUpdateParallel/Three_zone_update-2         	   10000	     96327 ns/op	    3366 B/op	     100 allocs/op
BenchmarkUpdateParallel/Three_zone_update-4         	   10000	     71151 ns/op	    3272 B/op	      95 allocs/op
BenchmarkUpdateParallel/Three_zone_update-6         	   10000	     52558 ns/op	    3079 B/op	      88 allocs/op
BenchmarkUpdateParallel/Three_zone_update-8         	   10000	     40569 ns/op	    2944 B/op	      84 allocs/op
BenchmarkUpdateParallel/Three_zone_update-16        	   10000	     23588 ns/op	    2827 B/op	      76 allocs/op
BenchmarkUpdateParallel/Three_zone_update-32        	   10000	     13253 ns/op	    2752 B/op	      72 allocs/op
BenchmarkUpdateParallel/Four_zone_update
BenchmarkUpdateParallel/Four_zone_update-2          	   10000	    116253 ns/op	    3705 B/op	     115 allocs/op
BenchmarkUpdateParallel/Four_zone_update-4          	   10000	     73139 ns/op	    3682 B/op	     111 allocs/op
BenchmarkUpdateParallel/Four_zone_update-6          	   10000	     54798 ns/op	    3557 B/op	     103 allocs/op
BenchmarkUpdateParallel/Four_zone_update-8          	   10000	     44023 ns/op	    3525 B/op	      98 allocs/op
BenchmarkUpdateParallel/Four_zone_update-16         	   10000	     24884 ns/op	    3340 B/op	      91 allocs/op
BenchmarkUpdateParallel/Four_zone_update-32         	   10000	     14308 ns/op	    3192 B/op	      87 allocs/op
PASS
ok  	tzresolver/src/pkg/tzcore	80.230s
PASS
ok  	tzresolver/src/pkg/utils	0.258s
