rm -rf ./build_artifacts/*
go build -o ./build_artifacts/ src/cmd/tzresolver/tzresolver.go
scripts/stop-server.sh	
go test -v -run=XXX -benchmem -bench=Benchmark --benchtime=10000x -cpu 2,4,6,8,16,32 ./...
?   	tzresolver/src/cmd/client	[no test files]
?   	tzresolver/src/cmd/tzresolver	[no test files]
PASS
ok  	tzresolver/src/pkg/api	0.220s
PASS
ok  	tzresolver/src/pkg/codec	0.095s
?   	tzresolver/src/pkg/dal	[no test files]
badger 2021/08/24 08:31:23 INFO: All 0 tables opened in 0s
badger 2021/08/24 08:31:23 INFO: Replaying file id: 0 at offset: 0
badger 2021/08/24 08:31:23 INFO: Replay took: 13.528µs
badger 2021/08/24 08:31:23 DEBUG: Value log discard stats empty
PASS
badger 2021/08/24 08:31:23 INFO: DropAll called. Blocking writes...
badger 2021/08/24 08:31:23 INFO: Writes flushed. Stopping compactions now...
badger 2021/08/24 08:31:23 INFO: Deleted 0 SSTables. Now deleting value logs...
badger 2021/08/24 08:31:23 INFO: Value logs deleted. Creating value log file: 0
badger 2021/08/24 08:31:23 INFO: Deleted 1 value log files. DropAll done.
badger 2021/08/24 08:31:23 INFO: Resuming writes
badger 2021/08/24 08:31:23 INFO: Got compaction priority: {level:0 score:1.73 dropPrefixes:[]}
ok  	tzresolver/src/pkg/dal/badgerdal	0.193s
?   	tzresolver/src/pkg/db/badgerdb	[no test files]
?   	tzresolver/src/pkg/server	[no test files]
PASS
ok  	tzresolver/src/pkg/timeutils	0.090s
goos: darwin
goarch: amd64
pkg: tzresolver/src/pkg/tzcore
cpu: Intel(R) Core(TM) i9-9880H CPU @ 2.30GHz
BenchmarkDeleteSerial
BenchmarkDeleteSerial/Single_zone_delete
BenchmarkDeleteSerial/Single_zone_delete-2          	   10000	    100815 ns/op	    2019 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-4          	   10000	    110695 ns/op	    2031 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-6          	   10000	    110304 ns/op	    2047 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-8          	   10000	    111725 ns/op	    2063 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-16         	   10000	    108908 ns/op	    2063 B/op	      66 allocs/op
BenchmarkDeleteSerial/Single_zone_delete-32         	   10000	    108780 ns/op	    2080 B/op	      66 allocs/op
BenchmarkDeleteSerial/Double_zone_delete
BenchmarkDeleteSerial/Double_zone_delete-2          	   10000	    101313 ns/op	    2342 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-4          	   10000	    114290 ns/op	    2352 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-6          	   10000	    112287 ns/op	    2353 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-8          	   10000	    112851 ns/op	    2353 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-16         	   10000	    116191 ns/op	    2401 B/op	      79 allocs/op
BenchmarkDeleteSerial/Double_zone_delete-32         	   10000	    112587 ns/op	    2401 B/op	      79 allocs/op
BenchmarkDeleteSerial/Three_zone_delete
BenchmarkDeleteSerial/Three_zone_delete-2           	   10000	    105942 ns/op	    3022 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-4           	   10000	    115682 ns/op	    3047 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-6           	   10000	    117537 ns/op	    3048 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-8           	   10000	    116379 ns/op	    3048 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-16          	   10000	    117303 ns/op	    3048 B/op	      93 allocs/op
BenchmarkDeleteSerial/Three_zone_delete-32          	   10000	    115885 ns/op	    3049 B/op	      93 allocs/op
BenchmarkDeleteSerial/Four_zone_delete
BenchmarkDeleteSerial/Four_zone_delete-2            	   10000	    104159 ns/op	    3282 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-4            	   10000	    117537 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-6            	   10000	    122941 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-8            	   10000	    125091 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-16           	   10000	    124667 ns/op	    3309 B/op	     105 allocs/op
BenchmarkDeleteSerial/Four_zone_delete-32           	   10000	    124388 ns/op	    3310 B/op	     105 allocs/op
BenchmarkDeleteParallel
BenchmarkDeleteParallel/Single_zone_delete
BenchmarkDeleteParallel/Single_zone_delete-2        	   10000	     86596 ns/op	    1946 B/op	      63 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-4        	   10000	     67041 ns/op	    1809 B/op	      57 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-6        	   10000	     48180 ns/op	    1633 B/op	      49 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-8        	   10000	     36456 ns/op	    1577 B/op	      46 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-16       	   10000	     17752 ns/op	    1341 B/op	      36 allocs/op
BenchmarkDeleteParallel/Single_zone_delete-32       	   10000	     10404 ns/op	    1256 B/op	      33 allocs/op
BenchmarkDeleteParallel/Double_zone_delete
BenchmarkDeleteParallel/Double_zone_delete-2        	   10000	     95071 ns/op	    2247 B/op	      76 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-4        	   10000	     71438 ns/op	    2187 B/op	      72 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-6        	   10000	     50064 ns/op	    2053 B/op	      64 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-8        	   10000	     35902 ns/op	    1929 B/op	      56 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-16       	   10000	     18167 ns/op	    1760 B/op	      48 allocs/op
BenchmarkDeleteParallel/Double_zone_delete-32       	   10000	     11196 ns/op	    1716 B/op	      46 allocs/op
BenchmarkDeleteParallel/Three_zone_delete
BenchmarkDeleteParallel/Three_zone_delete-2         	   10000	     98483 ns/op	    2928 B/op	      90 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-4         	   10000	     72911 ns/op	    2806 B/op	      84 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-6         	   10000	     51603 ns/op	    2628 B/op	      77 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-8         	   10000	     39460 ns/op	    2452 B/op	      72 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-16        	   10000	     19204 ns/op	    2150 B/op	      61 allocs/op
BenchmarkDeleteParallel/Three_zone_delete-32        	   10000	     12100 ns/op	    2082 B/op	      59 allocs/op
BenchmarkDeleteParallel/Four_zone_delete
BenchmarkDeleteParallel/Four_zone_delete-2          	   10000	    114779 ns/op	    3196 B/op	     102 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-4          	   10000	     72982 ns/op	    2936 B/op	      92 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-6          	   10000	     53518 ns/op	    2884 B/op	      89 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-8          	   10000	     38504 ns/op	    2773 B/op	      81 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-16         	   10000	     23832 ns/op	    2842 B/op	      77 allocs/op
BenchmarkDeleteParallel/Four_zone_delete-32         	   10000	     13064 ns/op	    2661 B/op	      71 allocs/op
BenchmarkReadSerial
BenchmarkReadSerial/Single_zone_reads
BenchmarkReadSerial/Single_zone_reads-2             	   10000	      1934 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-4             	   10000	      1440 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-6             	   10000	      1333 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-8             	   10000	      1316 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-16            	   10000	      1279 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Single_zone_reads-32            	   10000	      1249 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadSerial/Double_zone_reads
BenchmarkReadSerial/Double_zone_reads-2             	   10000	      2104 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-4             	   10000	      2122 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-6             	   10000	      2139 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-8             	   10000	      2157 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-16            	   10000	      1950 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Double_zone_reads-32            	   10000	      1940 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadSerial/Three_zone_reads
BenchmarkReadSerial/Three_zone_reads-2              	   10000	      2874 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-4              	   10000	      2675 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-6              	   10000	      2988 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-8              	   10000	      2895 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-16             	   10000	      2798 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Three_zone_reads-32             	   10000	      2993 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadSerial/Four_zone_reads
BenchmarkReadSerial/Four_zone_reads-2               	   10000	      3935 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-4               	   10000	      3480 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-6               	   10000	      3258 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-8               	   10000	      3453 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-16              	   10000	      3394 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadSerial/Four_zone_reads-32              	   10000	      3333 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel
BenchmarkReadParallel/Single_zone_reads
BenchmarkReadParallel/Single_zone_reads-2           	   10000	      1516 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-4           	   10000	      1100 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-6           	   10000	      1115 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-8           	   10000	      1141 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-16          	   10000	      1080 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Single_zone_reads-32          	   10000	      1080 ns/op	     520 B/op	      15 allocs/op
BenchmarkReadParallel/Double_zone_reads
BenchmarkReadParallel/Double_zone_reads-2           	   10000	      1367 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-4           	   10000	      1347 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-6           	   10000	      1374 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-8           	   10000	      1280 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-16          	   10000	      1229 ns/op	     984 B/op	      26 allocs/op
BenchmarkReadParallel/Double_zone_reads-32          	   10000	      1227 ns/op	     985 B/op	      26 allocs/op
BenchmarkReadParallel/Three_zone_reads
BenchmarkReadParallel/Three_zone_reads-2            	   10000	      1637 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-4            	   10000	      1302 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-6            	   10000	      1373 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-8            	   10000	      1324 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-16           	   10000	      1338 ns/op	    1424 B/op	      36 allocs/op
BenchmarkReadParallel/Three_zone_reads-32           	   10000	      1299 ns/op	    1425 B/op	      36 allocs/op
BenchmarkReadParallel/Four_zone_reads
BenchmarkReadParallel/Four_zone_reads-2             	   10000	      1899 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-4             	   10000	      1757 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-6             	   10000	      1491 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-8             	   10000	      1329 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-16            	   10000	      1343 ns/op	    1848 B/op	      46 allocs/op
BenchmarkReadParallel/Four_zone_reads-32            	   10000	      1352 ns/op	    1849 B/op	      46 allocs/op
BenchmarkUpdateSerial
BenchmarkUpdateSerial/Single_zone_update
BenchmarkUpdateSerial/Single_zone_update-2          	   10000	    103203 ns/op	    2300 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-4          	   10000	    114885 ns/op	    2347 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-6          	   10000	    117990 ns/op	    2363 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-8          	   10000	    119896 ns/op	    2378 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-16         	   10000	    115959 ns/op	    2380 B/op	      71 allocs/op
BenchmarkUpdateSerial/Single_zone_update-32         	   10000	    117458 ns/op	    2396 B/op	      71 allocs/op
BenchmarkUpdateSerial/Double_zone_update
BenchmarkUpdateSerial/Double_zone_update-2          	   10000	    112936 ns/op	    2704 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-4          	   10000	    115872 ns/op	    2750 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-6          	   10000	    117777 ns/op	    2750 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-8          	   10000	    122196 ns/op	    2747 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-16         	   10000	    114561 ns/op	    2799 B/op	      87 allocs/op
BenchmarkUpdateSerial/Double_zone_update-32         	   10000	    115872 ns/op	    2801 B/op	      87 allocs/op
BenchmarkUpdateSerial/Three_zone_update
BenchmarkUpdateSerial/Three_zone_update-2           	   10000	    103151 ns/op	    3456 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-4           	   10000	    116618 ns/op	    3503 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-6           	   10000	    120063 ns/op	    3501 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-8           	   10000	    118971 ns/op	    3502 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-16          	   10000	    119387 ns/op	    3504 B/op	     104 allocs/op
BenchmarkUpdateSerial/Three_zone_update-32          	   10000	    129856 ns/op	    3504 B/op	     104 allocs/op
BenchmarkUpdateSerial/Four_zone_update
BenchmarkUpdateSerial/Four_zone_update-2            	   10000	    110208 ns/op	    3783 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-4            	   10000	    120072 ns/op	    3825 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-6            	   10000	    122519 ns/op	    3825 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-8            	   10000	    118284 ns/op	    3826 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-16           	   10000	    118493 ns/op	    3829 B/op	     119 allocs/op
BenchmarkUpdateSerial/Four_zone_update-32           	   10000	    120689 ns/op	    3829 B/op	     119 allocs/op
BenchmarkUpdateParallel
BenchmarkUpdateParallel/Single_zone_update
BenchmarkUpdateParallel/Single_zone_update-2        	   10000	     85723 ns/op	    2229 B/op	      67 allocs/op
BenchmarkUpdateParallel/Single_zone_update-4        	   10000	     68532 ns/op	    2142 B/op	      62 allocs/op
BenchmarkUpdateParallel/Single_zone_update-6        	   10000	     48353 ns/op	    1984 B/op	      55 allocs/op
BenchmarkUpdateParallel/Single_zone_update-8        	   10000	     38159 ns/op	    1891 B/op	      51 allocs/op
BenchmarkUpdateParallel/Single_zone_update-16       	   10000	     23536 ns/op	    1736 B/op	      44 allocs/op
BenchmarkUpdateParallel/Single_zone_update-32       	   10000	     11030 ns/op	    1659 B/op	      39 allocs/op
BenchmarkUpdateParallel/Double_zone_update
BenchmarkUpdateParallel/Double_zone_update-2        	   10000	     92055 ns/op	    2619 B/op	      83 allocs/op
BenchmarkUpdateParallel/Double_zone_update-4        	   10000	     66347 ns/op	    2524 B/op	      76 allocs/op
BenchmarkUpdateParallel/Double_zone_update-6        	   10000	     51665 ns/op	    2461 B/op	      72 allocs/op
BenchmarkUpdateParallel/Double_zone_update-8        	   10000	     40166 ns/op	    2402 B/op	      67 allocs/op
BenchmarkUpdateParallel/Double_zone_update-16       	   10000	     21970 ns/op	    2267 B/op	      59 allocs/op
BenchmarkUpdateParallel/Double_zone_update-32       	   10000	     12442 ns/op	    2192 B/op	      56 allocs/op
BenchmarkUpdateParallel/Three_zone_update
BenchmarkUpdateParallel/Three_zone_update-2         	   10000	     99120 ns/op	    3365 B/op	     100 allocs/op
BenchmarkUpdateParallel/Three_zone_update-4         	   10000	     73320 ns/op	    3315 B/op	      96 allocs/op
BenchmarkUpdateParallel/Three_zone_update-6         	   10000	     54231 ns/op	    3079 B/op	      88 allocs/op
BenchmarkUpdateParallel/Three_zone_update-8         	   10000	     41343 ns/op	    2943 B/op	      83 allocs/op
BenchmarkUpdateParallel/Three_zone_update-16        	   10000	     23885 ns/op	    2812 B/op	      76 allocs/op
BenchmarkUpdateParallel/Three_zone_update-32        	   10000	     13299 ns/op	    2737 B/op	      72 allocs/op
BenchmarkUpdateParallel/Four_zone_update
BenchmarkUpdateParallel/Four_zone_update-2          	   10000	    102135 ns/op	    3706 B/op	     115 allocs/op
BenchmarkUpdateParallel/Four_zone_update-4          	   10000	     76667 ns/op	    3666 B/op	     109 allocs/op
BenchmarkUpdateParallel/Four_zone_update-6          	   10000	     54261 ns/op	    3549 B/op	     102 allocs/op
BenchmarkUpdateParallel/Four_zone_update-8          	   10000	     43304 ns/op	    3478 B/op	      98 allocs/op
BenchmarkUpdateParallel/Four_zone_update-16         	   10000	     24855 ns/op	    3287 B/op	      91 allocs/op
BenchmarkUpdateParallel/Four_zone_update-32         	   10000	     13936 ns/op	    3183 B/op	      87 allocs/op
PASS
ok  	tzresolver/src/pkg/tzcore	80.858s
PASS
ok  	tzresolver/src/pkg/utils	0.203s
