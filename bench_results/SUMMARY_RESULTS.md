# Summary of Benchmarks Run

## Baselines

Initially, a DAL was created using a purely in-memory golang map (hash table) with 2 granularities of locking:
1. Lock per key - results in: in-memory-hash-key-level-locks.txt
2. Lock per entire hash - results in: in-memory-kv-store-full-db-lock.txt

Note: A lot of this code was ported and moved to different parts of the code base to enable mocking of the current
      implementation of the DAL, which uses the Badger Embbeded DB.

### In-memory has (baseline) compared with BadgerDB(Full ACID)
There is far too much data in the results, but one thing is clear is that once we increase the
number of clients beyond the number of cores on my laptop, little changes in terms of perf.

Also, batch reads/writes/updates were tested with batches sizes of 1,2,3,4. The summary below
highlights results for batches of size 4.

| In memory hash                          | Key-Level Locking   | Hash-Level Locking  | BadgerDB-Full-ACID          |   |
|-----------------------------------------|---------------------|---------------------|-----------------------------|---|
| -----                                   | -----------------   | ------------------  | ------------------          |   |
| Serial reads of 4 keys:                 | 1174 ns per read    | 2146 ns per read    | 3519 ns per read            |   |
| Parallel reads of 4 keys (8 clients):   | 290.7 ns per read   | 567.4 ns per read   | 1274 ns per read            |   |
| Serial updates of 4 keys:               | 781 ns per update   | 1725 ns per update  | 117375 ns per read (117 ms) |   |
| Parallel updates of 4 keys (8 clients): | 834.4 ns per update | 588.1 ns per update | 43508 ns per read (435 ms)  |   |
| Serial deletes of 4 keys:               | 380.7 ns per delete | 835.9 ns per delete | 125806 ns per read (126 ms) |   |
| Parallel deletes of 4 keys (8 clients): | 673.2 ns per delete | 188.7 ns per delete | 38923 ns per read (38.9 ms) |   |

### Lessons Learned
1. Using BadgerDB, while gainging all the ACID properties, only loses between a factor of 2-4x slowdown for reads.
2. Deletes/updats have a consirable slowdown by factors upto 1000x in some cases. Though, tuning and more experimentation is needed.
3. Setting the worker pool for TCP connections (where each worker will also do independent accesses to embbeded DB)
   to the number of CPU cores seems to be the inflection point beyond with we reach the law of diminishing returns.
   Note: This was not proven in the analysis above, this comes from staring at the raw results.

This application most likely has data access patterns with lots of reads, and comparatively less writes - how often 
would someone need to add or update a zone? 
As such, the current code has the worker pool size set to numCPUs and the embbedded badger DB in full ACID mode enabled (sync writes to disk).

### Shortcuts / Gaps in Analysis
0. No analysis of memory and golang profiling is presented here. Just did not have enough time this weekend.
1. Benchmarks do not interleave reads/updates/deletes
2. There is tonnes of raw data, the analysis shown above is very basic. 
3. The compaction / garbage-colleciton functions of BadgerDB were not called (in fact are not called in the code at all).
   So the only compaction / garbage-collection happens at database opening and closing.
   This did enable a bendhmark analysis that did not require analysis of tails of distributions (thus the single average numbers
   presented above) though.
4. The benchmarks did not open real TCP connections to the application.  Instead, they used a lowel layer (tzcore).
   So the golang benchmark framework when running parallel clients creates client threads that embed the tzcore directly in them.
5. Need some awk scripts and gnu plot to properly present the data instead of the short summary chart above.
