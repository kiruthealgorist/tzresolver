# Very simple Makefile only
PORT=6666 # Port that server will run on

# Run unit tests
test:
	go test -v -short ./...

# Run unit tests with coverage and race detection
test-cover:
	go test -v -short -cover -coverprofile=coverage.txt -covermode=atomic \
		-coverpkg=$(shell go list) \
		-race \
		./...

# Run component tests (starts real server and connects using TCP connections)
component-test: build stop-server init-zones run-background
	go test -v -run ComponentTests ./...

# Stop any running tzresolver server
stop-server:
	scripts/stop-server.sh	

# Clean up tzresolver binary
clean:
	rm -f ./build_artifacts/tzresolver

# Do a clean build (binary goes in build_artifacts directory)
build: clean
	go build -o ./build_artifacts/ src/cmd/tzresolver/tzresolver.go

# Create the initial db with the default zones/offsets in there
init-zones: build stop-server
	INIT_DEFAULT_ZONES=true build_artifacts/tzresolver

# Run application in foreground
run-server: build stop-server
	PORT=${PORT} build_artifacts/tzresolver

# Run application in background
run-background: build stop-server
	PORT=${PORT} build_artifacts/tzresolver &

# Run benchmarks (currently application is wired to embed BadgerDB in full ACID config)
bench: build stop-server
	go test -v -run=XXX -benchmem -bench=Benchmark --benchtime=10000x -cpu 2,4,6,8,16,32 ./...
